<!-- https://mermaid.live -->

```mermaid
%%{init: {'theme': 'neutral'}}%%

classDiagram
    %% Classes
    class Tache {
        -static_counter_id
        -nom
        -duree_total
        -etat
        -dependances
        +unique_id
        +visite

        +realise()
        +depends_from(tache)
        +ajouteDependance(tache)
        +dureeParal()
        +PP_postfixe(liste_tache)
    }

    class Projet {
        #tache_fin
        #tache_debut
        #taches
        -cleanMarks()*
        #pick_two_random_tasks()
        #contains(id ou name)
        #topologicalSort()
        +consult_tasks()
    }
    class ProtoProjet {
        -cleanMarks()
        +ajoute(nom, duree)
        +ajoute(nom, duree, id)
        +ajoute(nom, duree, id1, id2)
    }
    class RunProjet {
        +run(id ou liste_tache)
    }

    class Gestionnaire {
        -salaire
        +payer(argent)
        +avis(RunProjet)*
    }
    class Consultant {
        +avis(RunProjet)
    }
    class Expert {
        +avis(RunProjet)
    }

    %% Héritage
    Projet <|-- ProtoProjet
    ProtoProjet <|-- RunProjet

    Gestionnaire <|-- Consultant
    Gestionnaire <|-- Expert

    %% Friends (Dependency)
    %% pour : consult_tasks()
    Projet ..> Consultant
    Projet ..> Expert

    %% Composition
    Tache --* Projet
```
