#ifndef TP5_PROTOPROJET_HPP
#define TP5_PROTOPROJET_HPP 1

#include "Projet.hpp"

class ProtoProjet : public Projet {
  void cleanMarks() const;

  friend std::ostream &operator<<(std::ostream &, const ProtoProjet &);

public:
  ProtoProjet();          // constructor
  virtual ~ProtoProjet(); // destructor

  ProtoProjet(const ProtoProjet &);                  // copy constructor
  const ProtoProjet &operator=(const ProtoProjet &); // copy assignement

  // Insère une nouvelle tâche en la plaçant entre deux tâches au hasard
  bool ajoute(const std::string nom, const int duree);

  // Insère une nouvelle tâche en la plaçant après la tâche avec l'ID
  // renseignée et avant la tâche finale, en créant une dépendance
  bool ajoute(const std::string nom, const int duree, const int id);

  // Insère une nouvelle tâche en la plaçant entre les deux tâches renseignée
  // via ID
  bool ajoute(const std::string nom, const int duree, const int id1,
              const int id2);
};

#endif
