#ifndef TP5_RUNPROJET_HPP
#define TP5_RUNPROJET_HPP 1

#include "ProtoProjet.hpp"

struct Consultant;
struct Expert;

class RunProjet final : protected ProtoProjet {
  friend std::ostream &operator<<(std::ostream &, const RunProjet &);

  // Ami pour pouvoir consulter les tâches
  friend Consultant;
  friend Expert;

public:
  RunProjet() = delete;     // remove default constructor
  RunProjet(ProtoProjet &); // constructor
  virtual ~RunProjet();     // destructor

  RunProjet(const RunProjet &);                  // copy constructor
  const RunProjet &operator=(const RunProjet &); // copy assignement

  // Lance une tâche
  bool run(const int id) const;

  // Exécute une liste de tâches dans l'ordre donnée
  void run(const std::vector<const Tache *> sequence_taches) const;
};

#endif
