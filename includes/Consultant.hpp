#ifndef TP5_CONSULTANT_HPP
#define TP5_CONSULTANT_HPP 1

#include "Gestionnaire.hpp"

class Consultant final : public Gestionnaire {
  friend std::ostream &operator<<(std::ostream &, const Consultant &);

public:
  Consultant();          // constructor
  virtual ~Consultant(); // destructor

  Consultant(const Consultant &);                  // copy constructor
  const Consultant &operator=(const Consultant &); // copy assignement

  std::pair<std::vector<int>, int> avis(const RunProjet &projet) const;
};

#endif
