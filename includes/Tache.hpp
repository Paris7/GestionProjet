#ifndef TP5_TACHE_HPP
#define TP5_TACHE_HPP 1

#include <iostream>
#include <vector>

class Tache final {
  // Compteur global pour les IDs
  static int counter_id;

  // Etat
  enum Etat { EnAttente, Realisee };
  friend std::ostream &operator<<(std::ostream &, const Etat &);

  // Nom de la tâche
  std::string name;
  // Durée totale pour faire la tâche
  int duree_total;
  // Etat actuelle de la tâche
  enum Etat etat;
  // Liste des dépendances
  std::vector<Tache *> dependances;

  friend std::ostream &operator<<(std::ostream &, const Tache &);

  // Auxilliaire pour simplifier les copies
  void _copy(const Tache &);

public:
  // ID unique de la tâche
  const int unique_id;
  // Vrai si la tâche à été visitée par le parcours en profondeur
  bool visite;

  Tache(const std::string, const int); // constructor
  virtual ~Tache();                    // destructor

  Tache(const Tache &);                  // copy constructor
  const Tache &operator=(const Tache &); // copy assignement

  // Déclenche la réalisation de la tâche après vérification
  bool realise();

  // Indique si la tâche courante dépend d'une autre
  bool depends_from(const Tache &) const;

  // Ajoute une dépendance si possible
  bool ajouteDependance(Tache &);

  // Calcule la durée totale max de réalisation d'une tâche et des dépendances
  int dureeParal() const;

  // Parcours en profondeur
  void PP_postfixe(std::vector<Tache *> &);

  // Récupère la durée totale
  int get_duree_totale() const;

  // Récupère le nom
  std::string get_name() const;
};

#endif
