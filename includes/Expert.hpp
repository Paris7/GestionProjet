#ifndef TP5_EXPERT_HPP
#define TP5_EXPERT_HPP 1

#include "Gestionnaire.hpp"

class Expert final : public Gestionnaire {
  friend std::ostream &operator<<(std::ostream &, const Expert &);

public:
  Expert();          // constructor
  virtual ~Expert(); // destructor

  Expert(const Expert &);                  // copy constructor
  const Expert &operator=(const Expert &); // copy assignement

  std::pair<std::vector<int>, int> avis(const RunProjet &projet) const;
};

#endif
