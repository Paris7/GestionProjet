#ifndef TP5_GESTIONNAIRE_HPP
#define TP5_GESTIONNAIRE_HPP 1

#include "RunProjet.hpp"

class Gestionnaire {
  int salaire_attendu;
  int salaire_recu;

  // Renvoie le reste à payer pour donner une expertise
  int reste_a_payer() const;

  friend std::ostream &operator<<(std::ostream &, const Gestionnaire &);

protected:
  // Auxiliaire pour simplifier l'affichage d'un projet
  std::ostream &print(std::ostream &) const;

  // Vrai si Gestionnaire à été payé.e
  bool payer() const;

public:
  Gestionnaire();          // constructor
  virtual ~Gestionnaire(); // destructor

  Gestionnaire(const Gestionnaire &);                  // copy constructor
  const Gestionnaire &operator=(const Gestionnaire &); // copy assignement

  // Paye Gestionnaire, renvoie le montant restant à payer
  int payer(const int argent);

  // Renvoie l'ordonnancement et la durée totale restante
  // Renvoie une liste vide et une durée totale -1 si pas payé.e
  virtual std::pair<std::vector<int>, int> avis(const RunProjet &) const = 0;
};

#endif
