#ifndef TP5_PROJET_HPP
#define TP5_PROJET_HPP 1

#include "Tache.hpp"

#include <algorithm>

struct RunProjet;

class Projet {
  // Auxilliaire pour simplifier les copies
  void _copy(const Projet &);

  // Auxiliiaire pour simplifier la libération de mémoire des tâches
  void free_taches();

  // Remet tous les marquages à leur valeur initiale
  virtual void cleanMarks() const = 0;

  friend std::ostream &operator<<(std::ostream &, const Projet &);
  friend RunProjet;

protected:
  Tache
      // Source du graphe aka la dernière à être exécutée
      *fin,

      // Première tâche qui sera exécutée
      *debut;

  Projet();          // constructor
  virtual ~Projet(); // destructor

  Projet(const Projet &);                  // copy constructor
  const Projet &operator=(const Projet &); // copy assignement

  // Graphe acyclique
  std::vector<Tache *> taches;

  // Auxiliaire pour simplifier l'affichage d'un projet
  std::ostream &print(std::ostream &) const;

  // Retourne une paire d'indentifiants de tâches au hasard
  const std::pair<const int, const int> pick_two_random_tasks() const;

  // Indique pour une tâche si elle fait partie du projet
  Tache *contains(const int id) const;
  Tache *contains(const std::string name) const;

  // Corrige les éventuelles anomalies du vector de tâches
  void topologicalSort();

public:
  // Donne une version du vecteur de tâches non modifiable
  const std::vector<const Tache *> consult_tasks() const;
};

#endif
