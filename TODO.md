# TP5

TODO avant rendu :

- [x] Page rédigée montrant que l'on a tenu compte des problématiques de copies et sécurité (private/public) en expliquant en français non technique comment vous les avez traitées (`x` à pas accès à `y`)
- [x] Écrire des tests pour montrer que ça fonctionne

## Notes

- [x] Toute classe doit être surchargée avec `<<`
- [x] Gestion satisfaisante des copies, affectation, destruction

### Projet

- [x] Ne dois **pas** être instanciée directement
- [x] Se décompose en tâches (vecteur de tâches)
- [x] Durée totale dépend du degré de parallélisme que le gestionnaire décide
- [x] Deux natures de projets dépendantent de `Projet`
  - En cours d'élaboration (`ProtoProjet`)
  - En cours d'exécution (`RunProjet`)
- [x] Peut être vu comme un graphe acyclique (utilisation de `vector`) des tâches
  - Les sommets sont des tâches
  - Une tâche `fin` servira de source au graphe
- Garder un ordre topologique (triée par dépendances)
- [x] Mère des deux classes `ProtoProjet` et `RunProjet`
- [x] Méthodes utiles qu'à ses sous-classes
- Méthodes (cf. le PDF du prof)
  - [x] `pick_two_random_tasks()`
  - [x] `contains()`
  - [x] Surcharge de `<<` : afficher toutes les tâches
  - [x] `consult_tasks()`
  - [x] `topologicalSort()`

#### ProtoProjet

- Permet de pouvoir ajouter de nouvelles tâches
- Ne peut pas progresser (a.k.a ne peut pas lancer les tâches)
- Champs
  - [x] Tâche `début`
  - [x] Tâche `fin`
- Méthodes (cf. le PDF du prof) **⇒ Tout ça avec l'ordre topologique**

  - Pas de méthode d'ajout d'un objet `Tache`
  - [x] `bool ajoute(nom, durée)` : sélectionne au hasard 2 tâches déjà
        enregistrer et **ajoute** la nouvelle tâche entres-elles
  - [x] `bool ajoute(nom, durée, id)` : **ajoute** une tâche qui doit se réaliser
        **après** la tâche qui à l'`id` correspondant (et avant la tâche finale)
  - [x] `bool ajoute(nom, durée, id1, id2)` : **ajoute** une tâche entre les 2 tâches
        qui ont l'identifiant `id1` et `id2`
  - [x] Surcharge de `<<`

#### RunProjet

- [x] Construit uniquement via un `ProtoProjet`
- [x] Avance vers sa conclusion en prenant en compte des tâches ponctuelles
- [x] Vide le contenu d'un `ProtoProjet` pour se construire, rendre les tâches "read-only"
- Méthodes (cf. le PDF du prof)
  - [x] `run(id)` : Lance une tâche
  - [x] `run(sequence_taches)` : Exécute une liste de tâches dans l'ordre donnée
  - [x] Surcharge de `<<`

### Concepteur

L'utilisateur final en somme

- [x] Interagis avec un `ProtoProjet` pour y mettre des tâches

### Gestionnaire

- [x] Étudie/Analyse des `RunProjet`

- [x] Recommande des ordonnancements pour la suite des exécutions à venir
      (ordre d'exécutions des tâches)
- [x] Calcule la durée totale restante d'un projet
- [x] Demande un salaire pour travailler
- Méthodes
  - [x] Surcharge de `<<` : Affiche une facturation
  - [x] `pair<vector<int>, int> avis(const RunProjet &)` : renvoie l'ordonnancement
        et la durée totale restante

#### Consultant

- [x] Calcule **sans** parallélisation des tâches

#### Expert

- [x] Calcule **avec** parallélisation des tâches

### Tâches

- [x] Dépendantes, entre-elles
- [x] Durée exacte, fixe, propre à elle-même
- [x] Ne peut être lancé que si toutes les dépendances sont réalisées
- [x] Dépendances = autres tâches
- [x] Élément atomique (final)
- Champs
  - [x] Nom
  - [x] Numéro unique
  - [x] État (réalisée/en attente)
    - Réalisée → En attente : **interdit**
    - En attente → Réalisée : **autorisé**
  - [x] Vision locale des dépendances (`vector`)
- Méthodes (cf. le PDF du prof)
  - [x] `bool realise()`
  - [x] `bool depends_from(Tache & x)`
  - [x] `bool ajouteDependance(Tache & x)`
  - [x] `int dureeParal()`
  - [x] Surcharge de `<<`
