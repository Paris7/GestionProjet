CXX      = g++
CXXFLAGS = --std=c++11
RM       = rm
TAR      = tar -cf

SOURCES = $(wildcard src/*.cpp)
OBJETS  = $(patsubst %.cpp,%.cpp.o,$(notdir $(SOURCES)))

EXE     = tp5
EXE_EXT = out

PDF_LOCATION = report
PDF_FILE     = explications.pdf

ARCHIVE = $(EXE).tar

%.cpp.o: src/%.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(DEVFLAGS)

main: CXXFLAGS += -O3
main: compilation

dev: CXXFLAGS += -Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic -g
dev: CXXFLAGS += -Wold-style-cast -Wsign-conversion
dev: compilation

compilation: $(OBJETS)
	$(CXX) -o $(EXE).$(EXE_EXT) $(OBJETS)

all:
	main

pdf-make:
	cd report && \
	$(MAKE)

pdf-clean:
	cd report && \
	$(MAKE) clean

clean: pdf-clean
	-$(RM) $(OBJETS) "$(EXE).$(EXE_EXT)" "$(ARCHIVE)"

archive: pdf-make
	cp "$(PDF_LOCATION)/$(PDF_FILE)" .
	$(TAR) "$(ARCHIVE)" $(SOURCES) $(wildcard includes/*.hpp) Makefile \
	                    binome.txt "$(PDF_FILE)" diagramme_uml.png
	$(RM) "$(PDF_FILE)"
