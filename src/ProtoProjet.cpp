#include "../includes/ProtoProjet.hpp"

ProtoProjet::ProtoProjet() {}

ProtoProjet::~ProtoProjet() {}

ProtoProjet::ProtoProjet(const ProtoProjet &src) : Projet(src) {}

const ProtoProjet &ProtoProjet::operator=(const ProtoProjet &src) {
  if (this == &src) {
    return *this;
  }

  return *this;
}

std::ostream &operator<<(std::ostream &out, const ProtoProjet &data) {
  return data.print(out);
}

void ProtoProjet::cleanMarks() const {
  for (auto t : taches) {
    t->visite = false;
  }
};

bool ProtoProjet::ajoute(const std::string nom, const int duree) {
  // Sélection de 2 tâches au hasard
  auto tasks = pick_two_random_tasks();

  // Ajout des tâches
  return ajoute(nom, duree, tasks.first, tasks.second);
}

bool ProtoProjet::ajoute(const std::string nom, const int duree, const int id) {
  // Ajout de la tâche entre l'ID voulu et la tâche finale
  return ajoute(nom, duree, id, fin->unique_id);
}

bool ProtoProjet::ajoute(const std::string nom, const int duree, const int id1,
                         const int id2) {
  Tache *tache1 = contains(id1);
  Tache *tache2 = contains(id2);

  // Vérifie que les tâches avec les id1 et id2 existent bien dans la liste
  if (!tache1 || !tache2) {
    return false;
  }

  // Création des dépendances
  Tache *tache = new Tache(nom, duree);
  if (!tache1->ajouteDependance(*tache) && !tache->ajouteDependance(*tache2)) {
    // Problème de dépendance
    delete tache;
    return false;
  }

  // Mise à jour du vecteur avec tri topologique
  taches.push_back(tache);
  topologicalSort();
  return true;
}
