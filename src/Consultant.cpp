#include "../includes/Consultant.hpp"

Consultant::Consultant() {}

Consultant::~Consultant() {}

Consultant::Consultant(const Consultant &) : Gestionnaire() {}

const Consultant &Consultant::operator=(const Consultant &src) {
  if (this == &src) {
    return *this;
  }

  return *this;
}

std::ostream &operator<<(std::ostream &out, const Consultant &data) {
  return data.print(out);
}

std::pair<std::vector<int>, int>
Consultant::avis(const RunProjet &projet) const {
  std::vector<int> ordonnancement;
  if (!payer()) {
    return std::make_pair(ordonnancement, -1);
  }

  const std::vector<const Tache *> taches = projet.consult_tasks();
  int duree_totale = 0;
  for (const Tache *it : taches) {
    duree_totale += it->get_duree_totale();
  }

  for (const Tache *it : taches) {
    ordonnancement.push_back(it->unique_id);
  }

  return std::make_pair(ordonnancement, duree_totale);
}
