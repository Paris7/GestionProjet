#include "../includes/RunProjet.hpp"

RunProjet::RunProjet(ProtoProjet &src) : ProtoProjet(src) { src.free_taches(); }

RunProjet::~RunProjet() {
  // Ici on veut supprimes les tâches
  for (Tache *it : taches) {
    delete it;
  }
}

RunProjet::RunProjet(const RunProjet &src) : ProtoProjet(src) {}

const RunProjet &RunProjet::operator=(const RunProjet &src) {
  if (this == &src) {
    return *this;
  }

  return *this;
}

std::ostream &operator<<(std::ostream &out, const RunProjet &data) {
  return data.print(out);
}

bool RunProjet::run(const int id) const {
  Tache *tache = contains(id);
  // La tâche n'existe pas
  if (!tache) {
    return false;
  }

  // Renvoie si la tâche s'est réalisée
  return tache->realise();
}

void RunProjet::run(const std::vector<const Tache *> sequence_taches) const {
  for (const Tache *it : sequence_taches) {
    run(it->unique_id);
  }
}
