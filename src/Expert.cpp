#include "../includes/Expert.hpp"

Expert::Expert() {}

Expert::~Expert() {}

Expert::Expert(const Expert &) : Gestionnaire() {}

const Expert &Expert::operator=(const Expert &src) {
  if (this == &src) {
    return *this;
  }

  return *this;
}

std::ostream &operator<<(std::ostream &out, const Expert &data) {
  return data.print(out);
}

std::pair<std::vector<int>, int> Expert::avis(const RunProjet &projet) const {
  std::vector<int> ordonnancement;
  if (!payer()) {
    return std::make_pair(ordonnancement, -1);
  }

  const std::vector<const Tache *> taches = projet.consult_tasks();
  const int duree_totale = taches.front()->dureeParal();

  for (const Tache *it : taches) {
    ordonnancement.push_back(it->unique_id);
  }

  return std::make_pair(ordonnancement, duree_totale);
}
