#include "../includes/Gestionnaire.hpp"

Gestionnaire::Gestionnaire() {
  salaire_attendu = rand() % 500 + 400; // Entre 400 et 899
  salaire_recu = 0;
}

Gestionnaire::~Gestionnaire() {}

Gestionnaire::Gestionnaire(const Gestionnaire &src)
    : salaire_attendu(src.salaire_attendu), salaire_recu(0) {}

const Gestionnaire &Gestionnaire::operator=(const Gestionnaire &src) {
  if (this == &src) {
    return *this;
  }

  salaire_attendu = src.salaire_attendu;
  salaire_recu = src.salaire_recu;
  return *this;
}

std::ostream &Gestionnaire::print(std::ostream &out) const {
  out << "La facture s'élève à " << salaire_attendu << "€, il reste "
      << reste_a_payer() << "€ à payer";

  return out;
}

std::ostream &operator<<(std::ostream &out, const Gestionnaire &data) {
  return data.print(out);
}

int Gestionnaire::reste_a_payer() const {
  return salaire_attendu - salaire_recu;
}

bool Gestionnaire::payer() const {
  bool return_val = salaire_recu >= salaire_attendu;

  if (!return_val) {
    std::cerr << "Je n'ai pas été payé. " << *this << ".\n";
  }

  return return_val;
}

int Gestionnaire::payer(const int argent) {
  if (reste_a_payer() <= 0) {
    return 0;
  }

  salaire_recu += argent;
  if (reste_a_payer() <= 0) {
    return 0;
  }

  return reste_a_payer();
}
