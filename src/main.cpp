#include "../includes/Consultant.hpp"
#include "../includes/Expert.hpp"

using namespace std;

// Fait des tests pour vérifier/montrer que ça fonctionne
void test();

// Transforme un vecteur<int> en string
std::string vecint_tostring(std::vector<int> list);

int main() {
  /* test();
  return EXIT_SUCCESS; */

  ProtoProjet pp;

  pp.ajoute("a", 10); // probablement numero 2
  cout << pp;         // avec ses 3 taches
  cout << "\n----------" << endl;

  pp.ajoute("b", 15, 0, 1); // en supposant que 0: fin et 1: début
  cout << pp;
  cout << "\n----------" << endl;

  RunProjet rp{pp};
  cout << "----- verification : ProtoProjet vide " << endl;
  cout << pp << endl;

  Consultant c;
  c.payer(1000);
  cout << c.avis(rp).second << endl; // dira 25

  Expert e;
  e.payer(1000);
  cout << e.avis(rp).second << endl; // dira 15

  return EXIT_SUCCESS;
}

void test() {
  /********************************** Projet **********************************/
  // Projet p; // impossible car classe abstraite

  /******************************* ProtoProjet  *******************************/
  ProtoProjet pp;
  cout << "pp: " << pp << "\n";
  /* pp: [
   *   Tâche(#0, "Fin", Attend)      => 1 dépendances,
   *   Tâche(#1, "Début", Attend)    => 0 dépendances,
   * ] */

  pp.ajoute("a", 10);       // ajoute une dépendance à #1 le "début"
  pp.ajoute("b", 15, 0, 1); // ajoute une dépendance à #0 (id1)
  cout << "pp: " << pp << "\n";
  /* pp: [
   *   Tâche(#0, "Fin", Attend)      => 2 dépendances,
   *   Tâche(#3, "b", Attend)        => 0 dépendances,
   *   Tâche(#1, "Début", Attend)    => 1 dépendances,
   *   Tâche(#2, "a", Attend)        => 0 dépendances,
   * ]
   *
   * => L'ordre pour les dépendances est respecté */

  /******************************** RunProjet  ********************************/
  // RunProjet rp; // impossible car le constructeur vide n'existe pas
  RunProjet rp(pp);
  cout << "pp: " << pp << "\n";
  /* pp: []
   *
   * => La liste est bien vidée*/

  cout << "rp: " << rp << "\n";
  /* rp: [
   *   Tâche(#0, "Fin", Attend)      => 2 dépendances,
   *   Tâche(#3, "b", Attend)        => 0 dépendances,
   *   Tâche(#1, "Début", Attend)    => 1 dépendances,
   *   Tâche(#2, "a", Attend)        => 0 dépendances,
   * ]
   *
   * => Les mêmes tâches ont étés transférés à RunProjet (pas de copie vérifiés
   *    par les ID identiques) */

  /******************************** Consultant  *******************************/
  Consultant c;
  cout << "c: " << c << "\n";
  /* c: La facture s'élève à 693€, il reste 693€ à payer
   *
   * => Affiche la facture */

  auto avis = c.avis(rp);
  cout << "c(avis): (" << vecint_tostring(avis.first) << ", " << avis.second
       << ")\n";
  /* Je n'ai pas été payé. La facture s'élève à 693€, il reste 693€ à payer.
   * c(avis): ([], -1)
   *
   * => Iel ne donne pas d'avis car pas payé. */

  cout << "c(payer 1000): " << c.payer(1000) << "\n";
  /* c(payer 1000): 0
   *
   * On donne 1000 pour être sûr qu'iel soit content
   * Il renvoie 0 car iel a plus besoin d'être payé */

  auto avis2 = c.avis(rp);
  cout << "c(avis): (" << vecint_tostring(avis2.first) << ", " << avis2.second
       << ")\n";
  /* c(avis): ([0, 3, 1, 2], 25)
   *
   * => Iel donne la liste d'exécution
   *    et le temps d'exécution sans parallélisation */

  /********************************** Expert  *********************************/
  Expert e;
  cout << "e: " << e << "\n";
  /* e: La facture s'élève à x€, il reste x€ à payer
   *
   * => Affiche la facture */

  auto avis3 = e.avis(rp);
  cout << "e(avis): (" << vecint_tostring(avis3.first) << ", " << avis3.second
       << ")\n";
  /* Je n'ai pas été payé. La facture s'élève à y€, il reste y€ à payer.
   * e(avis): ([], -1)
   *
   * => Iel ne donne pas d'avis car pas payé. */

  cout << "e(payer 1000): " << e.payer(1000) << "\n";
  /* e(payer 1000): 0
   *
   * On donne 1000 pour être sûr qu'iel soit content
   * Il renvoie 0 car iel a plus besoin d'être payé */

  auto avis4 = e.avis(rp);
  cout << "e(avis): (" << vecint_tostring(avis4.first) << ", " << avis4.second
       << ")\n";
  /* e(avis): ([0, 3, 1, 2], 15)
   *
   * => Iel donne la liste d'exécution
   *    et le temps d'exécution avec parallélisation */

  /* A la fin, la mémoire est correctement libérée, testé via Valgrind-3.21.0 :
   * HEAP SUMMARY:
   *     in use at exit: 0 bytes in 0 blocks
   *   total heap usage: 33 allocs, 33 frees, 75,550 bytes allocated
   * All heap blocks were freed -- no leaks are possible
   * ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0) */
}

std::string vecint_tostring(std::vector<int> list) {
  std::string return_value = "[";
  if (list.empty()) {
    return return_value + "]";
  }
  for (auto it : list) {
    return_value += std::to_string(it) + ", ";
  }

  // \b\b pour retirer la dernière virgule
  return return_value + "\b\b]";
}
