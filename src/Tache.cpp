#include "../includes/Tache.hpp"

int Tache::counter_id = 0;

Tache::Tache(const std::string n, const int duree)
    : name(n), duree_total(duree), etat(EnAttente),
      dependances(std::vector<Tache *>()), unique_id(counter_id++),
      visite(false) {}

Tache::~Tache() {}

void Tache::_copy(const Tache &src) {
  // Copie des dépendances
  dependances.reserve(src.dependances.size());
  for (Tache *t : src.dependances) {
    dependances.push_back(t);
  }
}

Tache::Tache(const Tache &src)
    : name(src.name), duree_total(src.duree_total), etat(src.etat),
      unique_id(counter_id++), visite(src.visite) {
  _copy(src);
}

const Tache &Tache::operator=(const Tache &src) {
  if (this == &src) {
    return *this;
  }
  name = src.name;
  duree_total = src.duree_total;
  etat = src.etat;
  visite = src.visite;
  _copy(src);
  return *this;
}

std::ostream &operator<<(std::ostream &out, const Tache::Etat &data) {
  switch (data) {
  case Tache::EnAttente:
    out << "Attend";
    break;

  case Tache::Realisee:
    out << "Réalisée";
    break;
  }
  return out;
}

std::ostream &operator<<(std::ostream &out, const Tache &data) {
  out << "Tâche(#" << data.unique_id << ", \"" << data.name << "\", "
      << data.etat << ")\t=> " << data.dependances.size() << " dépendances";
  return out;
}

bool Tache::realise() {
  for (const Tache *const it : dependances) {
    if (it->etat != Realisee) {
      // Une dépendance n'est pas réalisée, donc on peut pas se réaliser
      // soi-même
      return false;
    }
  }

  // TODO: Faire une vérification si le temps est écoulée ?
  // Dans ce cas là faut stocker le temps d'exécution actuelle de la tâche ?

  etat = Realisee;
  return true;
}

bool Tache::depends_from(const Tache &x) const {
  for (const Tache *const it : dependances) {
    if (it->unique_id == x.unique_id) {
      return true;
    } else {
      // On recherche récursivement parmis toutes les dépendance. Ca permet à
      // `ajouteDependance` d'être sûre de pas créer de cycles au n-ème degré
      if (it->depends_from(x)) {
        return true;
      }
    }
  }
  return false;
}

bool Tache::ajouteDependance(Tache &x) {
  // On ne créer pas de dépendances cyclique aka 2 tâches
  // qui dépendent l'une de l'autre
  if (x.depends_from(*this)) {
    return false;
  }

  dependances.push_back(&x);
  return true;
}

int Tache::dureeParal() const {
  int ret_value = duree_total,
      // La durée max permet d'éviter d'ajouter plusieurs fois une même
      // dépendance
      max_duree = 0;

  // On regarde toutes nos dépendances
  for (const Tache *const it : dependances) {
    // On demande la durée max de la dépendance
    int val = it->dureeParal();
    // De toutes nos dépendances on garde la valeur la plus haute
    if (val > max_duree) {
      max_duree = val;
    }
  }

  // On ajoute la valeur max à notre durée totale
  return ret_value + max_duree;
}

void Tache::PP_postfixe(std::vector<Tache *> &pile) {
  visite = true;
  for (Tache *it : dependances) {
    if (!it->visite) {
      it->PP_postfixe(pile);
    }
  }

  // Moment post-fix du parcours
  pile.push_back(this);
}

int Tache::get_duree_totale() const { return duree_total; }

std::string Tache::get_name() const { return name; }
