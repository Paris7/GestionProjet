#include "../includes/Projet.hpp"

Projet::Projet() : fin(new Tache("Fin", 0)), debut(new Tache("Début", 0)) {
  fin->ajouteDependance(*debut);

  taches.push_back(fin);
  taches.push_back(debut);
}

Projet::~Projet() { free_taches(); }

void Projet::_copy(const Projet &src) {
  // Copie des tâches
  taches.reserve(src.taches.size());
  for (Tache *t : src.taches) {
    taches.push_back(t);
  }
}

void Projet::free_taches() { taches.clear(); }

Projet::Projet(const Projet &src) : fin(src.fin), debut(src.debut) {
  _copy(src);
}

const Projet &Projet::operator=(const Projet &src) {
  if (this == &src) {
    return *this;
  }
  fin = src.fin;
  debut = src.debut;
  free_taches();
  _copy(src);
  return *this;
}

std::ostream &Projet::print(std::ostream &out) const {
  // Liste vide
  if (taches.empty()) {
    out << "[]";
    return out;
  }

  out << "[\n";
  for (Tache *t : taches) {
    out << "  " << *t << ",\n";
  }
  out << ']';

  return out;
}

std::ostream &operator<<(std::ostream &out, const Projet &data) {
  return data.print(out);
}

const std::pair<const int, const int> Projet::pick_two_random_tasks() const {
  // Choix aléatoire d'une tâche
  size_t size = this->taches.size();
  size_t rand1 = static_cast<size_t>(rand()) % size;
  size_t rand2 = static_cast<size_t>(rand()) % size;

  Tache *tache1 = this->taches[rand1];
  Tache *tache2 = this->taches[rand2];

  // tache2 ne doit pas dépendre transitivement de tache1
  if (tache2->depends_from(*tache1)) {
    return pick_two_random_tasks();
  }

  int id1 = tache1->unique_id;
  int id2 = tache2->unique_id;
  return std::make_pair(id1, id2);
}

Tache *Projet::contains(const int id) const {
  for (Tache *t : this->taches) {
    if (id == t->unique_id) {
      return t;
    }
  }

  return nullptr;
}

Tache *Projet::contains(const std::string name) const {
  for (Tache *t : this->taches) {
    if (name == t->get_name()) {
      return t;
    }
  }

  return nullptr;
}

const std::vector<const Tache *> Projet::consult_tasks() const {
  return std::vector<const Tache *>(taches.begin(), taches.end());
}

void Projet::topologicalSort() {
  // Construction de la pile
  std::vector<Tache *> pile;
  for (Tache *it : taches) {
    if (!it->visite) {
      it->PP_postfixe(pile);
    }
  }

  // Nettoyage des marques
  cleanMarks();

  // Reverse de la pile
  std::reverse(pile.begin(), pile.end());

  // Modifie la liste des tâches topologiquement triée
  taches = pile;
}
